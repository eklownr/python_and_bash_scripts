from googletrans import Translator
import argparse
 
parser = argparse.ArgumentParser()
parser.add_argument("prompt", help="The prompt to send to google translate. For example: \'hello world\' ")
args = parser.parse_args()

t = Translator()
anser = t.translate(args.prompt, dest="en")
print (anser.text)


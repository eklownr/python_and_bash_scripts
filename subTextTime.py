import sys

def open_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    return lines


def write_file(file_path, lines):
    with open(file_path, 'w') as file:
        file.writelines(lines)


# Set new time in seconds 00:00:00,123
def read_lines(lines: str,  sec: int) -> list:
    new_line = []
    for line in lines:
        if line.__contains__('-->'):
            split_line = line.split('-->')
            start = split_line[0].split(",")
            end = split_line[1].split(",")
            new_line.append(new_time(start[0], sec) + "," + start[1] + "--> " + new_time(end[0], sec) + "," + end[1])
        else:
            new_line.append(line)
    return new_line


def new_time(time: str, sec: int) -> str:
    h, m, s = map(int, time.split(':'))
    total_seconds = h * 3600 + m * 60 + s # Convert the time to seconds
    new_total_seconds = total_seconds + sec
    
    # Convert the new total seconds back to hours, minutes, and seconds
    new_h = new_total_seconds // 3600
    new_m = (new_total_seconds % 3600) // 60
    new_s = new_total_seconds % 60
    
    # Format the new time as a string
    new_time = f"{new_h:02d}:{new_m:02d}:{new_s:02d}"
    
    return new_time


def main():
    arg = sys.argv
    if len(arg) > 1:
        new_list = read_lines(open_file(arg[1]), int(arg[2]))
    else:
        print("usage: python3 subTextTime.py <file_path> <sec>")
        sys.exit(1)

    write_file("new_"+ arg[2]+ "_" + arg[1], new_list)



if __name__ == "__main__":
    main()
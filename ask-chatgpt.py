import requests
import argparse
import mykey

parser = argparse.ArgumentParser()
parser.add_argument("prompt", help="The prompt to send to the OpenAI API")
args = parser.parse_args()

api_endpoint = "https://api.openai.com/v1/completions"
api_key = mykey.get_key() 

request_headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + api_key
}

# code-davinci-002
request_data = {
    "model": "text-davinci-003",
    "prompt": f"{args.prompt}",
    "max_tokens": 1500,
    "temperature": 0.8
}
respons = requests.post(api_endpoint, headers=request_headers, json=request_data)

if respons.status_code == 200:
    print(respons.json()['choices'][0]['text'])
else:
    print(f"Request failed! {str(respons.status_code)}")
    print(respons.json())


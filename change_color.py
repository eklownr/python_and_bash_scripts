import tkinter as tk
import time

# Create the window
root = tk.Tk()

# Create a canvas
canvas = tk.Canvas(root, width=300, height=300)
canvas.pack()

# Create a color list
color_list = ["red", "orange", "yellow", "green", "blue", "purple"]

# Create the counter
i = 0

# Create a function to change the color
def change_color():
    global i
    canvas.config(bg=color_list[i])
    i = (i+1) % len(color_list)
    root.after(1000, change_color)

# call the function
change_color()

# loop the window
root.mainloop()

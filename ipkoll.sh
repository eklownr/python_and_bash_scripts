#!/bin/bash
R='\033[0;31m'   #'0;31' is Red's ANSI color code
G='\033[0;32m'   #'0;32' is Green's ANSI color code
Y='\033[1;33m'   #'1;33' is Yellow's ANSI color code
B='\033[0;34m'   #'0;34' is Blue's ANSI color code
P='\033[1;35m'   #'1;35' is Light Purple ANSI color code
W='\033[1;37m'   #'1;37' is White ANSI color code


echo -e "$P****************************************************************"
echo -e "$Y\tIP-address: $W"
ip a | grep "inet " | awk '{ print $2; }' | lolcat -a


echo -e "$P****************************************************************"
echo -e "$Y\tDefault gateway: $W"
ip r | grep default | lolcat -a


echo -e "$P****************************************************************"
echo -e "$Y\tDNS: $W"
resolvectl status | grep "DNS Server" | lolcat -a


echo -e "$P****************************************************************"
echo -e "$Y\tRoute: $W"
route -n | lolcat -f
echo -e "$P****************************************************************$W"
